# @atlaskit/legacy-mobile-macros

## 0.1.1

### Patch Changes

- [`8a93403847`](https://bitbucket.org/atlassian/atlassian-frontend/commits/8a93403847) - readability updated
- Updated dependencies

## 0.1.0

### Minor Changes

- [`622ae0dc66`](https://bitbucket.org/atlassian/atlassian-frontend/commits/622ae0dc66) - [ux] added macros and dark theme support with query params configuration

### Patch Changes

- Updated dependencies
