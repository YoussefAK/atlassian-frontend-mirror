/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Chinese
export default {
  'pt.profile-card.closed.account': '已删除的帐户',
  'pt.profile-card.closed.account.has.date.a.few.months':
    '他们的帐户已被删除数月。',
  'pt.profile-card.closed.account.has.date.last.month':
    '他们的帐户已在上月删除。',
  'pt.profile-card.closed.account.has.date.more.than.a.year':
    '他们的帐户已被删除超过一年。',
  'pt.profile-card.closed.account.has.date.several.months':
    '他们的帐户已被删除数月。',
  'pt.profile-card.closed.account.has.date.this.month':
    '他的帐户已在本月删除。',
  'pt.profile-card.closed.account.has.date.this.week':
    '他们的帐户本周已被删除。',
  'pt.profile-card.closed.account.no.date': '他们的帐户已被删除。',
  'pt.profile-card.disabled.account.default.name': '前用户',
  'pt.profile-card.general.msg.disabled.user': '您无法再与此人合作。',
  'pt.profile-card.inactive.account': '帐户已停用',
  'pt.profile-card.inactive.account.has.date.a.few.months':
    '他们的帐户已被停用数月。',
  'pt.profile-card.inactive.account.has.date.last.month':
    '他们的帐户上月已被停用。',
  'pt.profile-card.inactive.account.has.date.more.than.a.year':
    '他们的帐户已被停用超过一年。',
  'pt.profile-card.inactive.account.has.date.several.months':
    '他们的帐户已被停用数月。',
  'pt.profile-card.inactive.account.has.date.this.month':
    '他们的帐户本月已被停用。',
  'pt.profile-card.inactive.account.has.date.this.week':
    '他们的帐户本周已被停用。',
  'pt.profile-card.inactive.account.no.date': '他们的帐户已被停用。',
};
