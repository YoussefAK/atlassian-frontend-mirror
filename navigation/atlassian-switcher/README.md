# Component template

This is where you make changes to the Atlassian switcher.

# Are you thinking of making a change to the Switcher?

That's great! Please read our [engagement model](https://product-fabric.atlassian.net/wiki/spaces/YW/pages/1953169670/Engagement+model+with+Your+Work+Start+Switcher+Recent+Activity+and+Notification+Frontend+Experience+FY21) and [contribution model](https://product-fabric.atlassian.net/wiki/spaces/YW/pages/1997669504/Contribution+Model+for+Your+Work) if you would like to make changes or raise a [JSM request](https://product-fabric.atlassian.net/servicedesk/customer/portal/92).

You can also reach out to us us in [#help-start-switcher](https://atlassian.slack.com/archives/C01EKJ3S7R8)

We want to stay informed about your intentions before you raise the PRs because a few reasons.

1. If you are updating the component APIs, we would like to explore the possibility of you either using existing functionality or making the changes re-usable for other products. This will help to minimize the effort of updating switcher everytime a product needs some customization.
2. If you are adding a feature behind a feature flag, we would also like to hear from you! It is important that the feature flags work, but it is also important that they are cleaned up after an experiment is over!
3. If you talk to us early, it will also help us prioritise our time to review the PRs.

# Helpful Links & Documentation

WIP
