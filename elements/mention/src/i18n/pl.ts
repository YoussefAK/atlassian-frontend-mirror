/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Polish
export default {
  'fabric.elements.mentions.team.meantion.highlight.close.button.tooltip':
    'Odrzuć',
  'fabric.elements.mentions.team.member.50plus': 'Ponad 50 członków',
  'fabric.elements.mentions.team.member.50plus.including.you':
    'Ponad 50 członków, wliczając Ciebie',
  'fabric.elements.mentions.team.member.count':
    '{0, plural, one {{0} członek} few {{0} członków} many {{0} członków} other {{0} członka}}',
  'fabric.elements.mentions.team.member.count.including.you':
    '{0, plural, one {{0} członek razem z Tobą} few {{0} członków razem z Tobą} many {{0} członków razem z Tobą} other {{0} członka razem z Tobą}}',
  'fabric.elements.mentions.team.mention.highlight.description':
    'Przekieruj wszystkich na stronę za pomocą jednego kliknięcia. Nie masz zespołu?',
  'fabric.elements.mentions.team.mention.highlight.description.link':
    'Załóż go.',
  'fabric.elements.mentions.team.mention.highlight.title':
    'Wspomniano Twój zespół',
  'fabric.mention.error.defaultAction': 'Spróbuj ponownie za chwilę',
  'fabric.mention.error.defaultHeadline': 'Coś poszło nie tak',
  'fabric.mention.error.differentText': 'Spróbuj wpisać inny tekst',
  'fabric.mention.error.loginAgain':
    'Spróbuj się wylogować i zalogować ponownie',
  'fabric.mention.noAccess.label': 'Brak dostępu',
  'fabric.mention.noAccess.warning':
    'Użytkownik {name} nie zostanie powiadomiony, ponieważ nie ma dostępu',
  'fabric.mention.unknow.user.error': 'Nieznany użytkownik {userId}',
};
