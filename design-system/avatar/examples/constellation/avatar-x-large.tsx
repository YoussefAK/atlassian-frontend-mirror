import React from 'react';

import Avatar from '../../src';

export default function AvatarXLargeExample() {
  return (
    <div>
      <Avatar size="xlarge" />
      <Avatar size="xlarge" appearance="square" />
    </div>
  );
}
