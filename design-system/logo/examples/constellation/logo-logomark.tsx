import React from 'react';

import { B200, B400 } from '@atlaskit/theme/colors';

import { AtlassianIcon } from '../../src';

export default () => (
  <AtlassianIcon
    iconColor={B200}
    iconGradientStart={B400}
    iconGradientStop={B200}
  />
);
