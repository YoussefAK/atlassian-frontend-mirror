export default {
  EDITOR_MOUNTED: 'Editor Component Mount Time',
  PROSEMIRROR_RENDERED: 'ProseMirror Render Time',
  PROSEMIRROR_CONTENT_RENDERED: 'ProseMirror Content Render Time',
};
