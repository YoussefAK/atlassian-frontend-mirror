/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Polish
export default {
  'fabric.editor.collapseNode': 'Zwiń zawartość',
  'fabric.editor.expandDefaultTitle': 'Kliknij tutaj, aby rozwinąć...',
  'fabric.editor.expandNode': 'Rozwiń zawartość',
  'fabric.editor.expandPlaceholder': 'Nadaj temu rozwinięciu tytuł...',
  'fabric.editor.openLink': 'Otwórz łącze na nowej karcie',
};
