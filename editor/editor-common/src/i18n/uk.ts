/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Ukrainian
export default {
  'fabric.editor.collapseNode': 'Згорніть вміст',
  'fabric.editor.expandDefaultTitle': 'Натисніть, щоб розгорнути…',
  'fabric.editor.expandNode': 'Розгорніть вміст',
  'fabric.editor.expandPlaceholder': 'Назвіть розгорнутий вміст…',
  'fabric.editor.openLink': 'Відкрити посилання в новій вкладці',
};
