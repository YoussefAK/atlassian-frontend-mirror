/**
 * NOTE:
 *
 * This file is automatically generated by i18n-tools.
 * DO NOT CHANGE IT BY HAND or your changes will be lost.
 */
// Italian
export default {
  'fabric.editor.collapseNode': 'Comprimi contenuto',
  'fabric.editor.expandDefaultTitle': 'Clicca qui per espandere...',
  'fabric.editor.expandNode': 'Espandi contenuto',
  'fabric.editor.expandPlaceholder': 'Assegna un titolo a questa espansione...',
  'fabric.editor.openLink': 'Apri link in una nuova scheda',
};
